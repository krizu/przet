import matplotlib.pyplot as plt
from skimage import exposure
from skimage import data
from skimage.util import img_as_ubyte
from skimage.util import img_as_float32
from skimage import io
from skimage import color
import skimage.filters  as imflt
from skimage import feature
import numpy as np
import warnings
import skimage.morphology as mph
 
#---------------------------------------------------------------------------------------------------
def add_noise(noise_typ,image):
   
    if noise_typ == "gauss":
        shp= image.shape
        mean = 0
        var = 0.01
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,shp)
        #gauss = gauss.reshape(shp)
        noisy = image + gauss
        return noisy
   
    elif noise_typ=="uniform":
        k=0.25
        if image.ndim==2:
            row,col = image.shape
            noise = np.random.rand(row,col)      
        else:
            row,col,chan = image.shape
            noise = np.random.rand(row,col,chan)
        noisy = image + k*noise
        return noisy
     
    elif noise_typ == "s&p":
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
       
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
        for i in image.shape]
        out[coords] = 1
   
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
        for i in image.shape]
        out[coords] = 0
        return out
     
    elif noise_typ == "poisson":
        PEAK=20
        noisy = image+np.random.poisson(PEAK*image)/PEAK
        return noisy
     
    elif noise_typ =="speckle":
        if image.ndim==2:
            row,col = image.shape
            noise = np.random.randn(row,col)      
        else:
            row,col,chan = image.shape
            noise = np.random.randn(row,col,chan)
        noisy = image + image * 0.1*noise
        return noisy
 
#----------------------------------------------------------------------------------    
def test_noise(ntype,fname):
   
    img=io.imread(fname)
    img=img_as_float32(img)
    img2=add_noise(ntype,img)
     
    fig, (ax, ax2) = plt.subplots(1,2)
    ax.imshow(img,cmap='gray')  
    ax.set_title('original image')
    ax2.imshow(img2,cmap='gray')
    ax2.set_title('noisy image')
    ax.axis('off')
    ax2.axis('off')
    fig.tight_layout()
    plt.show(block=False)
 
#---------------------------------------------------------------------------------------------------
def contrast_stretch():
   
    img= data.moon()
    p2, p98 = np.percentile(img, (2, 98))
    #img2 = exposure.rescale_intensity(img, in_range=(p2, p98))
   
    img2=(img.astype(np.float32)-p2)*255/(p98-p2)
    img2[img2<0]=0; img2[img2>255]=255
    img2=img2.astype(np.uint8)
   
    fig, (ax,ax2) = plt.subplots(1,2)
    ax.imshow(img,cmap='gray')  
    ax.set_title('original image')
    ax.imshow(img,cmap='gray')
    ax2.set_title('stretched image')
    ax2.imshow(img2,cmap='gray')
    ax.axis('off');ax2.axis('off')
    #fig.tight_layout()
    plt.show(block=False)
   
    # Display histogram
    img2=img_as_float32(img2)
    img=img_as_float32(img)
    fig, ax = plt.subplots()
    ax.hist(img.ravel(), bins=256, histtype='bar', color='blue')
    ax.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0))
    ax.set_xlabel('Pixel intensity')
    ax.set_xlim(0, 1)
    ax.grid(True)
    plt.show(block=False)
   
    # Display cumulative distribution
    fig, ax = plt.subplots()
    cdf, bins = exposure.cumulative_distribution(img2, 256)
    ax.plot(bins, cdf, 'r')
    ax.grid(True)
    plt.show(block=False)
   
   
#---------------------------------------------------------------------------------------------------
def lowpass_filter(dname,fname):
   
    fname=dname+fname
    img=io.imread(fname)
    #img=data.camera()
    img=img_as_float32(img)
    img=add_noise('s&p',img)
    if img.ndim>2:
        img=color.rgb2gray(img)
    #img2=imflt.gaussian(img,sigma=2,mode='reflect')
    img2 = imflt.median(img, mph.disk(1))
   
    fig, (ax, ax2) = plt.subplots(1,2)
    ax.imshow(img,cmap='gray')  
    ax.set_title('original image')
    ax2.imshow(img2,cmap='gray')
    ax2.set_title('filtered image')
    ax.axis('off')
    ax2.axis('off')
    fig.tight_layout()
    plt.show(block=False)
 
#---------------------------------------------------------------------------------------------------
def highpass_filter(dname,fname):
   
    fname=dname+fname
    img=io.imread(fname)
    #img=data.camera()
   
    if img.ndim>2:
        img=color.rgb2gray(img)
    img=img_as_ubyte(img)
    #img2 = imflt.roberts(img)
    #img2 = imflt.sobel(img)
    #img2 = imflt.scharr(img)
    #img2 = imflt.prewitt(img)
    #img2 = imflt.laplace(img,3)
 
    img2=feature.canny(img, sigma=3, low_threshold=0.1*255,
    high_threshold=0.2*255)
   
    fig, (ax, ax2) = plt.subplots(1,2)
    ax.imshow(img,cmap='gray')  
    ax.set_title('coins image')
    ax2.imshow(img2,cmap='gray')
    ax2.set_title('filtered image')
    ax.axis('off')
    ax2.axis('off')
    fig.tight_layout()
    plt.show(block=False)
   
 
#---------------------------------------------------------------------------------------------------
def main():
    plt.close()
    warnings.filterwarnings("ignore")
    dname="..\\images\\input1\\"
    #test_noise('speckle',dname+'coins.png')
    #lowpass_filter(dname,"coins.png")
    highpass_filter(dname,"coins.png")
    #contrast_stretch()
    #fft_filter(dname,'moonlanding.tif')
 
#---------------------------------------------------------------------------------------------------    
if __name__ == "__main__":
    #Run as main program
    main()