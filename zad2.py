# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 01:20:29 2018

@author: Mihau
"""

import os
import warnings
import numpy as np
import skimage as skimg
from skimage import io
from skimage import color
from skimage import exposure
import pydicom as dicom
import cv2
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.image as mpimg
import matplotlib.patches as patches
from skimage.util import img_as_ubyte
from skimage.util import img_as_float32
from skimage import filters
from skimage import feature
from skimage import morphology
from skimage import transform as tf
from skimage import data
from PIL import Image



def zad2(path):
    img = io.imread(path)
    height = img.shape[0]
    width = img.shape[1]
    img2 = np.zeros((height,width), dtype= 'uint8')
    for i in np.arange(int(height/2),height):
        for j in np.arange(0,int(width/2)):
            a = img.item(i,j)
            img2.itemset((i-64,j+64), a)

    
    img_tran = tf.rotate(img2, -45, resize=False)
    im_show(img2,img_tran)
    plt.show()

def im_show(img,img2):
    fig, (ax,ax2) = plt.subplots(1,2)
    ax.imshow(img, cmap="gray")
    ax.set_title("normal")
    ax2.imshow(img2, cmap="gray")
    ax2.set_title("transformed")
    ax.axis('off')
    ax2.axis('off')


def main():
    data = "C:\\Users\\Krizu\\Desktop\\images\\input1\\"
    image = "cameraman.bmp"
    zad2(data+image)
    
    
if __name__ == "__main__":
    main()


