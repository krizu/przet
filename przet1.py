import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg
from skimage.util import img_as_ubyte
import warnings
from skimage import color
from skimage import io
import os
import pydicom as dicom
 
from skimage import transform as tf
from skimage import data
 
 
def img_read_1(name):
   
    fname=''.join(['..\\images\\input1\\',name])
    img=mpimg.imread(fname)
 
    if img.ndim==3:
        img=color.rgb2gray(img)
   
    img=img[:,::-1]
    print(img.shape)
    print(img.size)
    print(img.ndim)
    img=img_as_ubyte(img)
   
    fig, ax = plt.subplots()
    ax.imshow(img,cmap='gray')  
    #plt.xticks([]), plt.yticks([])
    plt.axis('off')
    plt.show()
    print('koniec')
#--------------------------------------------------------------
def img_read_2(name):
   
    fname=''.join(['..\\images\\input1\\',name])
    img=io.imread(fname)
   
    if img.ndim==3:
        img=color.rgb2gray(img)
   
    img=img_as_ubyte(img)
    img=img[::-1,:]
   
    fig, ax = plt.subplots()
    ax.imshow(img,cmap='gray')  
    #plt.xticks([]), plt.yticks([])
    ax.axis('off')
    plt.show()
    data_dir2='..\\images\\output\\'
    root,ext=os.path.splitext(name)
    print(data_dir2+root+'3'+ext)
    io.imsave(data_dir2+root+'3'+ext,img,cmap='gray')
#----------------------------------------------------------------
def img_read_3(fname):
 
    data_dir='..\\images\\input2\\'
    ds = dicom.dcmread(data_dir+fname)
    rows = int(ds.Rows)
    cols = int(ds.Columns)
    image=ds.pixel_array
    print('rows=',rows,' cols=',cols)
    if hasattr(ds,'NumberOfFrames'):
        frames = int(ds.NumberOfFrames)
        print('frames=',frames)
        plt.imshow(image[:,:,15],cmap='gray')
    else:
        plt.imshow(image,cmap='gray')
    plt.axis('off')
    plt.show()
    print('koniec')
    '''
   root,ext=os.path.splitext(fname)
   fname2=root+'3'+ext
   dicom.dcmwrite(data_dir+fname2,ds)
   '''
#-----------------------------------------------------------------------
def img_transform(ang,tr):
   
    img = data.camera()
    img=img_as_ubyte(img)
    img2=tf.rotate(img,ang,resize=True)
 
    #tr=[50,100]
    hh=img2.shape[0]+tr[0]
    ww=img2.shape[1]+tr[1]
    img3=np.zeros((hh,ww),dtype=np.float64)
    img3[tr[0]:,tr[1]:]=img2
   
    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(img3, cmap=plt.cm.gray)
   
    for a in ax:
        a.axis('off')  
#-----------------------------------------------------------------------
def main():
    warnings.filterwarnings("ignore")
    print('Hello in Spyder')
    #img_read_1('lena.png')
    #img_read_2('cameraman.bmp')
    #img_read_3('OCTvolume.dcm')
    img_transform(-45,[50,100])
#-----------------------------------------------------------------------    
if __name__ == "__main__":
    #Run as main program
    main()
