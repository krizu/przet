# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 01:07:09 2018

@author: Mihau
"""

import os
import warnings
import numpy as np
import skimage as skimg
from skimage import io
from skimage import color
from skimage import exposure
import pydicom as dicom
import cv2
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.image as mpimg
import matplotlib.patches as patches
from skimage.util import img_as_ubyte
from skimage.util import img_as_float32
from skimage import filters
from skimage import feature
from skimage import morphology
from skimage import transform as tf
from skimage import data
from PIL import Image


def zad1():
    img = np.zeros((512,512), dtype='uint8')
    x0 = 256.0
    y0 = 256.0
    a = 100.0
    b = 200.0
    for i in range(0,512):
        for j in range(0,512):
            if (((i-x0)/a)**2 + ((j-y0)/b)**2) <=1:
                img.itemset((i,j), 255)
                
                
    img2 = feature.canny(img)            
    im_show(img,img2)
    plt.show()
    
def im_show(img,img2):
    
    fig, (ax,ax2) = plt.subplots(1,2)
    ax.imshow(img, cmap="gray")
    ax.set_title("elipse")
    ax2.imshow(img2, cmap="gray")
    ax2.set_title("filtered")
    ax.axis('off')
    ax2.axis('off')
        



def main():
    zad1()


if __name__ == "__main__":
    main()










